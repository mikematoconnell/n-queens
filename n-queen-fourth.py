import copy

def make_board(n):
    board = []
    for x in range(n):
        board.append([])
        for y in range(n):
            board[x].append(None)
    return board

n = 10
board = make_board(n)
result = []

def pos_is_safe(board, col, n, og_row): # row = 1, col = 1
    
    # is there something in the column

    l_row = board[og_row]
    if board[og_row][col] != None:
        return False
        # check for diagnol
    for row in range(n):
        if board[row][col] != None:
            return False

    for item in l_row:
        if item != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) > i >= 0 and (n-1) > j >= 0:
        j += 1
        i += 1

        if board[j][i] != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) >= i > 0 and (n-1) > j >= 0:
        j += 1
        i -= 1
        if board[j][i] != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) > i >= 0 and (n-1) >= j > 0:
        j -= 1
        i += 1
        if board[j][i] != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) >= i > 0 and (n-1) >= j > 0:
        j -= 1
        i -= 1
        if board[j][i] != None:
            return False
    return True
    

def n_queens(board, row, n):
    # 
    if row == n:
        result.append(board)
        return
    for col in range(n):
        if pos_is_safe(board, col, n, row):
            board[row][col] = "Queen"

            n_queens(board, row + 1, n)
            board[row][col] = None
    return result


print(len(n_queens(board, 0, n)))

