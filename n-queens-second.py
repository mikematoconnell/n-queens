import copy
from pprint import pprint

def find_positions(n):
    main_list = []
    for x in range(n):
        main_list.append([])
        for y in range(n):
            main_list[x].append(None)
    return main_list

def pos_is_safe(board, col, n, og_row): # row = 1, col = 1
    
    # is there something in the column

    l_row = board[og_row]
    if board[og_row][col] != None:
        return False
        # check for diagnol
    for row in range(n):
        if board[row][col] != None:
            return False

    for item in l_row:
        if item != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) > i >= 0 and (n-1) > j >= 0:
        j += 1
        i += 1

        if board[j][i] != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) >= i > 0 and (n-1) > j >= 0:
        j += 1
        i -= 1
        if board[j][i] != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) > i >= 0 and (n-1) >= j > 0:
        j -= 1
        i += 1
        if board[j][i] != None:
            return False
    i = copy.deepcopy(col)
    j = copy.deepcopy(og_row)
    while (n-1) >= i > 0 and (n-1) >= j > 0:
        j -= 1
        i -= 1
        if board[j][i] != None:
            return False
    return True
    
    

def success(board, n):
    queen_count = 0
    for row in board:
        for item in row:
            if item == "queen":
                queen_count += 1
    if queen_count == n:
        return True
    return False

def place_piece(board, row, col):
    board[row][col] = "queen"

def four_queens(board, n, col_count, row_count, return_list):
    # end cases are if solution has been found
    return_list = []
    copy_board = copy.deepcopy(board)
    print("copy", copy_board)
    for row in range(row_count, n):
        for col in range(col_count, n):

            four_queens(copy_board, n, n_col, row_count, return_list)
            if pos_is_safe(copy_board, col, n, row):
                place_piece(copy_board, row, col)
                break
    if success(copy_board, n):
        return_list.append(copy_board)

    print(return_list)
    return return_list.sort()

n = 4
b = find_positions(n)
print("List of possible n-queens given", n, len(four_queens(b, n, 0, 0, [])))
