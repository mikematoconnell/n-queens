## Fourth:
Adapted my code from second and third in order to make sure I properly understanding n-queens/backtracking after doing the rod problem and the NeetCode walk through. This is far less efficient than the Third.

## Third:
Followed along with NeetCode on YouTube learning about backtracking and a different approach to checking validity of current position. 

## Second:
This DOES NOT WORK. 
This was my attempt where I based most of it off of the first one, but I realized that I was missing something significant but I wanted to keep the code for reference.

## First:
This was my first try at doing it and it became overly complicated where I attempted to increment column and row starts in order to get every possible scenario. Depending on changing a few lines of code either one some instances were returned, there was a recursion error, or there was an infinite loop.