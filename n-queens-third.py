col = set()
pos_diag = set()
neg_diag = set()
result = []
def make_board(n: int):
    board = [["."] *  n for i in range(n)]
    return board

n = 10
board = make_board(n)

def back_track(r):
    if r == n:
        copy = ["".join(row) for row in board]
        result.append(copy)
        return

    for c in range(n):
        if c in col or (r + c) in pos_diag or (r - c) in neg_diag:
            continue

        col.add(c)
        pos_diag.add(r + c)
        neg_diag.add(r - c)
        board[r][c] = "Q"

        back_track(r + 1)

        col.remove(c)
        pos_diag.remove(r + c)
        neg_diag.remove(r - c)
        board[r][c] = "."

    
    return result


print(len(back_track(0)))
print(result)


